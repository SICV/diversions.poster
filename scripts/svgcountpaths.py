#!/usr/bin/env python3

import argparse, json
from xml.etree import ElementTree as ET 


ap = argparse.ArgumentParser("")
ap.add_argument("input")
ap.add_argument("--polyline", default=False, action="store_true")
ap.add_argument("--selector", default=".//{http://www.w3.org/2000/svg}path")
ap.add_argument("--json", default=False, action="store_true")
ap.add_argument("--key", default="svgcountpaths")
ap.add_argument("--id", default=None)
ap.add_argument("--img", default=None)
args = ap.parse_args()

if args.polyline:
    args.selector = ".//{http://www.w3.org/2000/svg}polyline"

with open(args.input) as f:
    t = ET.parse(f)
    count = len(t.findall(args.selector))
    if args.json:
        data = {'id': args.id or args.input}
        data[args.key] = {'value': count, 'label': "{0}".format(count)}
        if args.img:
            data[args.key]['img'] = args.img
        print (json.dumps(data))
    else:
        print (count)
