#!/usr/bin/env python3

from csv import DictReader
import argparse, sys, os, json


def pfl (x):
    if x != None and x.strip():
        return float(x)
    else:
        return 0.0

def humanize_bytes(bytesize, precision=2):
    """
    Humanize byte size figures
    """
    abbrevs = (
        (1 << 50, 'PB'),
        (1 << 40, 'TB'),
        (1 << 30, 'GB'),
        (1 << 20, 'MB'),
        (1 << 10, 'kB'),
        (1, 'bytes')
    )
    if bytesize == 1:
        return '1 byte'
    for factor, suffix in abbrevs:
        if bytesize >= factor:
            break
    if factor == 1:
        precision = 0
    return '%.*f %s' % (precision, bytesize / float(factor), suffix)

ap = argparse.ArgumentParser("csv to json sortable")
ap.add_argument("src")
ap.add_argument("--inputpath", default="/media/murtaugh/DIVERSIONSX/2016/DIVERSIONS/scrape/mim/")
ap.add_argument("--outputpath", default="sample")
# ap.add_argument("--imagepath", default="/media/murtaugh/DIVERSIONSX/2016/DIVERSIONS/Diversies/Images/MIM_Instr_Africains/")
args = ap.parse_args()

output = []
with open(args.src) as f:
    for row in DictReader(f):
        # print (row, file=sys.stderr)
        item = {}
        ipath = row['image']
        item['id'] = os.path.join(args.outputpath, ipath)
        base, ext = os.path.splitext(ipath)
        item['img'] = os.path.join(args.outputpath, base + ".trim.jpg")
        item['inventory'] = row['inventoryNb']
        # item['N° d_inventaire'] = row['N° d_inventaire']
        # item['name'] = row['Nom de l_objet']
        item['name'] = row['objectName']
        item['title'] = row['objectTitle']
        item['dating'] = row['dating']
        isize = int(row['image_filesize'])
        item['image_filesize'] = {
            'value': isize,
            'label': humanize_bytes(isize)
        }
        item['dimensions'] = {
            'value': (pfl(row.get('d0')), pfl(row.get('d1')), pfl(row.get('d2'))),
            'label': row.get('dlabel')
        }
        output.append(item)
print (json.dumps(output, indent=2))
