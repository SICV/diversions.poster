#!/usr/bin/env python3
import argparse, sys, os
from csv import DictReader, DictWriter
from random import shuffle
from shutil import copyfile


ap = argparse.ArgumentParser("Copy ROWs of Csv and images")
ap.add_argument("src")
ap.add_argument("output")
ap.add_argument("--shuffle", default=False, action="store_true")
ap.add_argument("--limit", type=int, default=None)
ap.add_argument("--copyimages", default=False, action="store_true")
ap.add_argument("--filterimageswithsize", type=int, default=9134, help="used to remove placeholder images")
ap.add_argument("--inputpath", default="/media/murtaugh/DIVERSIONSX/2016/DIVERSIONS/scrape/mim/")
ap.add_argument("--outputpath", default="sample")
args = ap.parse_args()

rows = []
with open(args.src) as f:
    reader = DictReader(f)
    for row in reader:
        rows.append(row)
if args.shuffle:
    shuffle(rows)

print ("Read {0} rows".format(len(rows)), file=sys.stderr)
count = 0
with open(args.output, "w") as f:
    writer = DictWriter(f, fieldnames=reader.fieldnames)
    writer.writeheader()
    for row in rows:
        # Check / copy image
        filename = row['image']
        base, ext = os.path.splitext(filename)
        ipath = os.path.join(args.inputpath, filename)
        if args.filterimageswithsize != None:
            if os.path.getsize(ipath) == args.filterimageswithsize:
                print ("Skipping missing image", file=sys.stderr)
                continue
        if args.copyimages:
            opath = os.path.join(args.outputpath, base+".orig"+ext)
            copyfile(ipath, opath)

        writer.writerow (row)
        count += 1
        if args.limit != None and count>=args.limit:
            break
print ("Copied {0} rows".format(count), file=sys.stderr)