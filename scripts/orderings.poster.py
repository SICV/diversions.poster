#!/usr/bin/env python3

import os, sys, json, re
from math import floor
import datetime

from argparse import ArgumentParser
from PIL import Image
from reportlab.pdfgen.canvas import Canvas
import reportlab.lib.pagesizes
from reportlab.lib.units import inch, cm
from reportlab.pdfbase.ttfonts import TTFont, pdfmetrics
from reportlab.lib.utils import ImageReader
from reportlab.lib.colors import ghostwhite, grey, lightgrey



####
"""
support simple and expanded values

{ key: value }
key: {
    value: 23,
    label: '23'
}

"""

def value (x):
    if type(x) == dict and 'value' in x:
        return x['value']
    return x

def non_zero_value (x):
    val = x
    if type(x) == dict and 'value' in x:
        val = x['value']
    if type(val) == list or type(val) == tuple:
        return len([x for x in val if x]) > 0
    else:
        return val


def label (x):
    if type(x) == dict and 'label' in x:
        return x['label']
    return x











ap = ArgumentParser("")
ap.add_argument("--filterzeros", default=False, action="store_true")
ap.add_argument("--pagesize", default="A1")
# ap.add_argument("--imagespath", default="/media/murtaugh/DIVERSIONSX/2016/DIVERSIONS/Diversies/Images/MIM_Instr_Africains")
ap.add_argument("--imagespath", default="sample")
ap.add_argument("--output", default="output.pdf")
ap.add_argument("--font", default="OSP-DIN.ttf")
ap.add_argument("--columns", type=int, default=18)
ap.add_argument("--fontsize", type=float, default=14.0)
ap.add_argument("--groupborder", type=float, default=2.5, help="horizontal gap between groups")
ap.add_argument("--cellborder", type=float, default=0.10)
ap.add_argument("--pageborder", type=float, default=2.5)
ap.add_argument("--landscape", default=False, action="store_true")
ap.add_argument("data")

args = ap.parse_args()
pagesize = getattr(reportlab.lib.pagesizes, args.pagesize)


# DATA
with open (args.data) as f:
    data = json.load(f)

## Sizing

pagemargin = {
    'left': args.pageborder*cm,
    'right': args.pageborder*cm,
    'top': args.pageborder*cm,
    'bottom': args.pageborder*cm
}

cellmargin = {
    'left': args.cellborder*cm,
    'right': args.cellborder*cm,
    'top': args.cellborder*cm,
    'bottom': args.cellborder*cm
}
groupborder = args.groupborder*cm
if args.landscape:
    pageheight, pagewidth = pagesize
else:
    pagewidth, pageheight = pagesize

cols = args.columns
usablewidth = pagewidth - pagemargin['left'] - pagemargin['right'] - 2*groupborder
cellwidth = usablewidth / cols
cellheight = cellwidth
rows = int(floor( (pageheight - pagemargin['top'] - pagemargin['bottom']) / cellheight))
ccw = cellcontentswidth = cellwidth - cellmargin['left'] - cellmargin['right']
cch = cellcontentsheight = cellwidth - cellmargin['top'] - cellmargin['bottom']
groupcols = int(floor(cols / 3.0))


# FONTS
font =  TTFont('LabelFont', args.font)
pdfmetrics.registerFont(font)
# PDF
canvas = Canvas(args.output, pagesize=(pagewidth, pageheight))
boxfill = 1,0,1
textcolor = 0,0,0
canvas.setStrokeColorRGB(0.0,0.0,0.0)
canvas.setFont('LabelFont', args.fontsize)
print ("{0} items".format(len(data)), file=sys.stderr)


def drawImageBox (ipath, x, c, r, boxsize=100, drawBorder=False, valign="center"):
    cx = x + (c*cellwidth)
    cy = pageheight - pagemargin['top'] - ((r+1)*cellheight)
    try:
        im = Image.open(ipath)
        # boxsize sets the resolution of the image such that it fits in boxsize x boxsize pixels
        im.thumbnail((boxsize, boxsize), Image.ANTIALIAS)
        imw = (im.size[0]/boxsize) * ccw
        imh = (im.size[1]/boxsize) * cch
        # centering dx,dy
        if valign=="top":
            dx, dy = (ccw - imw) / 2, (cch - imh)
        elif valign=="center":
            dx, dy = (ccw - imw) / 2, (cch - imh) / 2
        else: # "bottom"
            dx, dy = (ccw - imw) / 2, 0

        # DRAW IMAGE
        canvas.drawImage(
            ImageReader(im),
            cx+dx+cellmargin['left'],
            cy+dy+cellmargin['bottom'],
            width=imw,
            height=imh,
            mask='auto' # https://stackoverflow.com/questions/1308710/transparency-in-pngs-with-reportlab-2-3#1625350
        )
        if drawBorder:
            canvas.rect(cx, cy, cellwidth, cellheight)
        return cx, cy, dx, dy, imw, imh
    except Exception as e:
        print ("Exception {0}".format(e))
        return cx, cy, 0, 0, cellwidth, cellheight

r = 0
for sortkey in "name inventory image_filesize dimensions dating red green blue gradient contours hough tex lex".split():
    # SORT
    if args.filterzeros:
        sdata = [x for x in data if non_zero_value(x.get(sortkey))]
    else:
        sdata = data[:]
    sdata.sort(key=lambda x: value(x[sortkey]))
    print ("[{0}] range from {1} to {2}".format(sortkey, label(sdata[0][sortkey]), label(sdata[-1][sortkey])))

    # split the data into 3 groups: min, middle, max
    # each group has groupcols items
    midpoint = len(sdata)//2
    midstart = midpoint - (groupcols//2)
    groups = sdata[0:groupcols], sdata[midstart:midstart+groupcols], sdata[-groupcols:]

    groupx = pagemargin['left']
    fillDataBox = False
    for gi, group in enumerate(groups):
        c= 0
        for i in group:
            ipath = i['img']
            cx, cy, dx, dy, imw, imh = drawImageBox(ipath, groupx, c, r+1, valign="top")
            if 'img' in i[sortkey]:
                drawImageBox(i[sortkey]['img'], groupx, c, r, valign="bottom")
            txt = label(i[sortkey])
            if txt:
                cx = groupx + (c*cellwidth)
                cy = pageheight - pagemargin['top'] - ((r+1)*cellheight)
                canvas.setStrokeColor(lightgrey)
                canvas.setLineWidth(0.05)
                canvas.rect(cx+dx+cellmargin['left'],cy+cellmargin['bottom'],imw, imh)
                canvas.setFillColorRGB(*textcolor)
                # canvas.drawCentredString(cx+(cellwidth/2), cy+(cellheight/2), "{0}".format(txt))
                canvas.drawCentredString(cx+dx+cellmargin['left']+(imw/2), cy+cellmargin['bottom']+(imh/2), "{0}".format(txt))
            c += 1
        groupx += groupborder + (cellwidth*groupcols)
    r += 2

canvas.showPage()
print("Saving to {0}".format(args.output), file=sys.stderr)
canvas.save()
