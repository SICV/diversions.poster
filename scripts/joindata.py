#!/usr/bin/env python3

import argparse, json, sys, glob

ap = argparse.ArgumentParser("combine data.json and csv derived data")
ap.add_argument("src")
ap.add_argument("input", nargs="*")
ap.add_argument("--inputglob", default=None, help="can take a glob")
args = ap.parse_args()

with open(args.src) as f:
    base = json.load(f)

byid = {}
for i in base:
    byid[i['id']] = i

if args.inputglob != None:
    inputs = glob.glob(args.inputglob)
else:
    inputs = args.input

for n in inputs:
    print ("Reading data from {0}".format(n), file=sys.stderr)
    with open(n) as f:
        data = json.load(f)
    did = data['id']
    d = byid[did]
    for key in data:
        if key != "id":
            if key in d:
                print ("Warning: Overwriting {0} in {1}".format(key, did), file=sys.stderr)
            d[key] = data[key]

print (json.dumps(base, indent=2))
