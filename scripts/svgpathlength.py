#!/usr/bin/env python3

import argparse, json, sys, math
from xml.etree import ElementTree as ET 
from svg.path import parse_path, Line # pip3 install svg.path


ap = argparse.ArgumentParser("")
ap.add_argument("input")
ap.add_argument("--selector", default=".//{http://www.w3.org/2000/svg}path")
ap.add_argument("--json", default=False, action="store_true")
ap.add_argument("--key", default="svgpathlength")
ap.add_argument("--id", default=None)
ap.add_argument("--img", default=None)
args = ap.parse_args()

with open(args.input) as f:
    t = ET.parse(f)
    length = 0.0
    for p in t.findall(args.selector):
        pathdata = p.attrib.get("d")
        path = parse_path(p.attrib.get("d"))
        # assuming paths are simple lines
        if len(path) == 1 and type(path[0]) == Line:
            # print (path, file=sys.stderr)
            line = path[0]
            x1, y1 = line.start.real, line.start.imag
            x2, y2 = line.end.real, line.end.imag
            dist = math.sqrt((x2-x1)**2 + (y2-y1)**2)
            length += dist
    length = int(length)
    if args.json:
        data = {'id': args.id or args.input}
        data[args.key] = {'value': length, 'label': "{0}".format(length)}
        if args.img:
            data[args.key]['img'] = args.img
        print (json.dumps(data))
    else:
        print (length)
