#!/usr/bin/env python3

import argparse
import os, json, sys
from csv import DictReader, DictWriter
from random import shuffle
from shutil import copyfile


ap = argparse.ArgumentParser("")
ap.add_argument("src")
ap.add_argument("output")
ap.add_argument("--imagepath", default="/media/murtaugh/DIVERSIONSX/2016/DIVERSIONS/Diversies/Images/MIM_Instr_Africains/")
ap.add_argument("--copytopath", default="sample")
ap.add_argument("--shuffle", default=False, action="store_true")
ap.add_argument("--limit", type=int, default=None)
args = ap.parse_args()

# E.5794-011 => E.5794-01_1.jpg
# E.5794-01.jpg

def inv_to_path(x):
    yield x + ".jpg"
    yield x + ".JPG"

rows_by_filename = {}
match = 0
total = 0
rows = []
with open(args.src) as f:
    reader = DictReader(f)
    for row in reader:
        total += 1
        ipath = row['Image standard']
        ipath = ipath.split("\\")[-1]
        rows_by_filename[ipath] = row

        inv = row['N° d_inventaire']
        for path in inv_to_path(inv):
            fpath = os.path.join(args.imagepath, path)
            if os.path.exists(fpath):
                print (inv, path)
                match += 1
                row['img'] = path
                rows.append(row)
                continue


print ("Matched {0} of {1}".format(match, total), file=sys.stderr)
if args.shuffle:
    shuffle(rows)
if args.limit:
    rows = rows[:args.limit]
with open(args.output, "w") as f:
    writer = DictWriter(f, fieldnames=["img"] + reader.fieldnames)
    writer.writeheader()
    for row in rows:
        if args.copytopath:
            fpath = os.path.join(args.imagepath, row['img'])
            topath = os.path.join(args.copytopath, row['img'])
            copyfile(fpath, topath)
        writer.writerow(row)
