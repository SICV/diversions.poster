#!/usr/bin/env python3

import os, sys, json, re
from math import floor
import datetime

from argparse import ArgumentParser
from PIL import Image
from reportlab.pdfgen.canvas import Canvas
import reportlab.lib.pagesizes
from reportlab.lib.units import inch, cm
from reportlab.pdfbase.ttfonts import TTFont, pdfmetrics
from reportlab.lib.utils import ImageReader

ap = ArgumentParser("")
ap.add_argument("--pagesize", default="A0")
ap.add_argument("--imagespath", default="/media/murtaugh/DIVERSIONSX/2016/DIVERSIONS/Diversies/Images/MIM_Instr_Africains")
ap.add_argument("--output", default="output.pdf")
ap.add_argument("--font", default="OSP-DIN.ttf")
ap.add_argument("--jsonpath", default="filemodifydate.json")
ap.add_argument("--columns", type=int, default=18)
ap.add_argument("--fontsize", type=float, default=14.0)

ap.add_argument("--groupborder", type=float, default=2.5, help="horizontal gap between groups")
ap.add_argument("--cellborder", type=float, default=0.10)
ap.add_argument("--pageborder", type=float, default=2.5)

args = ap.parse_args()
pagesize = getattr(reportlab.lib.pagesizes, args.pagesize)
print (pagesize)

# datetime
# parsed_date = datetime.datetime.strptime( mydate, "%Y:%m:%d %H:%M:%S" )
def parsedate (d):
    # 2010:11:25 17:33:12+01:00
    # map +01:00 to +0100 which strptime can grok as %z
    d = re.sub(r"\+(\d\d):(\d\d)$", r"+\1\2", d)
    return datetime.datetime.strptime(d, "%Y:%m:%d %H:%M:%S%z")


# DATA
with open (args.jsonpath) as f:
    data = json.load(f)

## Sizing

pagemargin = {
    'left': args.pageborder*cm,
    'right': args.pageborder*cm,
    'top': args.pageborder*cm,
    'bottom': args.pageborder*cm
}

cellmargin = {
    'left': args.cellborder*cm,
    'right': args.cellborder*cm,
    'top': args.cellborder*cm,
    'bottom': args.cellborder*cm
}
groupborder = args.groupborder*cm


pagewidth, pageheight = pagesize

cols = args.columns
usablewidth = pagewidth - pagemargin['left'] - pagemargin['right'] - 2*groupborder
cellwidth = usablewidth / cols
cellheight = cellwidth
rows = int(floor( (pageheight - pagemargin['top'] - pagemargin['bottom']) / cellheight))
ccw = cellcontentswidth = cellwidth - cellmargin['left'] - cellmargin['right']
cch = cellcontentsheight = cellwidth - cellmargin['top'] - cellmargin['bottom']

groupcols = int(floor(cols / 3.0))

# print ("Grid size: {0}x{1}, Cell size: {2} x {3}".format(cols, rows, ccw, cch))

# FONTS
font =  TTFont('LabelFont', args.font)
pdfmetrics.registerFont(font)
# PDF
canvas = Canvas(args.output, pagesize=pagesize)
boxfill = 1,0,1
textcolor = 0,0,0
canvas.setStrokeColorRGB(0.0,0.0,0.0)
canvas.setFont('LabelFont', args.fontsize)
print ("{0} items".format(len(data)), file=sys.stderr)



# SORT BY FileModifyDate
for i in data:
    i['FileModifyDate_parsed'] = parsedate(i['FileModifyDate'])
data.sort(key=lambda x: x['FileModifyDate_parsed'])
print ("data ranges from {0} to {1}".format(data[0]['FileModifyDate_parsed'], data[-1]['FileModifyDate_parsed']))

# split the data into 3 groups: min, middle, max
# each group has groupcols items
midpoint = len(data)//2
midstart = midpoint - (groupcols//2)
groups = data[0:groupcols], data[midstart:midstart+groupcols], data[-groupcols:]
# for x in groups:
#     assert (len(x) == groupcols)

groupx = pagemargin['left']
fillDataBox = False
for gi, group in enumerate(groups):
    c, r = 0, 0
    for i in group:
        ipath = os.path.join(args.imagespath, i['SourceFile'])
        im = Image.open(ipath)
        im.thumbnail((100, 100), Image.ANTIALIAS)
        imw = (im.size[0]/100) * ccw
        imh = (im.size[1]/100) * cch
        # centering dx,dy
        dx, dy = (ccw - imw) / 2, (cch - imh) / 2
        cx = groupx + (c*cellwidth)
        cy = pageheight - pagemargin['top'] - ((r+1+1)*cellheight)
        # DRAW IMAGE
        canvas.drawImage(ImageReader(im), cx+dx+cellmargin['left'], cy+dy+cellmargin['bottom'], width=imw, height=imh)
        # canvas.rect(cx, cy, cellwidth, cellheight)
        # DRAW DATA BOX
        cy = pageheight - pagemargin['top'] - ((r+1)*cellheight)
        if fillDataBox:
            canvas.setFillColorRGB(*boxfill)
            canvas.rect(cx+dx+cellmargin['left'], cy+dy+cellmargin['bottom'], imw, imh, stroke=0, fill=1)
        # drawTextCenteredInBox
        txt = i['FileModifyDate_parsed'].strftime("%Y-%m-%d %H:%M:%S")
        canvas.setFillColorRGB(*textcolor)
        canvas.drawCentredString(cx+dx+cellmargin['left']+(imw/2), cy+dy+cellmargin['bottom']+(imh/2), "{0}".format(txt))

        # print("cx, cy", cx, cy, file=sys.stderr)
        c += 1
    groupx += groupborder + (cellwidth*groupcols)

# for i in range(1000):
#     
#     c.setFont('MyFontName', 72)
#     # c.drawString(10*cm, 0.5*cm, "Page {0}".format(i))
#     c.drawCentredString(A4[0]/2, A4[1]/2, "Page {0}".format(i))
canvas.showPage()
print("Saving to {0}".format(args.output), file=sys.stderr)
canvas.save()
