#!/usr/bin/env python3

import argparse, json, re, sys

    # 165173: (  0,  0,  0,  0) #00000000 none
    #   1327: (  0,255,  0,255) #00FF00FF lime

ap = argparse.ArgumentParser("Count non black pixels from an image magick historgram")
ap.add_argument("--input", type=argparse.FileType('r'), default=sys.stdin)
ap.add_argument("--json", default=False, action="store_true")
ap.add_argument("--key", default="histcount")
ap.add_argument("--id", default=None)
ap.add_argument("--img", default=None)
args = ap.parse_args()

count = 0
for line in args.input:
    # print (line, file=sys.stderr)
    m = re.search(r"^ *(\d+):\s*\(\s*(\d+),\s*(\d+),\s*(\d+)\s*,\s*(\d+)\s*\)", line)
    if m:
        # print ("m.groups()", m.groups(), file=sys.stderr)
        cnt, r, g, b, a = [int(x) for x in m.groups()]
        if r > 0 or g> 0 or b>0:
             count += cnt

if args.json:
    data = {'id': args.id or args.input}
    data[args.key] = {'value': count, 'label': "{0}".format(count)}
    if args.img:
        data[args.key]['img'] = args.img
    print (json.dumps(data))
else:
    print (count)


