orig=$(shell ls sample/*.orig.jpg)
trim=$(orig:%.orig.jpg=%.trim.jpg)
contourssvg=$(trim:%.trim.jpg=%.contours.svg)
contourspng=$(trim:%.trim.jpg=%.contours.png)
contoursjson=$(trim:%.trim.jpg=%.contours.json)
gradientsvg=$(trim:%.trim.jpg=%.gradient.svg)
gradientpng=$(trim:%.trim.jpg=%.gradient.png)
gradientjson=$(trim:%.trim.jpg=%.gradient.json)
red=$(trim:%.trim.jpg=%.red.png)
redjson=$(trim:%.trim.jpg=%.red.json)
green=$(trim:%.trim.jpg=%.green.png)
greenjson=$(trim:%.trim.jpg=%.green.json)
blue=$(trim:%.trim.jpg=%.blue.png)
bluejson=$(trim:%.trim.jpg=%.blue.json)
texsvg=$(trim:%.trim.jpg=%.tex.svg)
texpng=$(trim:%.trim.jpg=%.tex.png)
texjson=$(trim:%.trim.jpg=%.tex.json)
lexsvg=$(trim:%.trim.jpg=%.lex.svg)
lexpng=$(trim:%.trim.jpg=%.lex.png)
lexjson=$(trim:%.trim.jpg=%.lex.json)
houghsvg=$(trim:%.trim.jpg=%.hough.svg)
houghpng=$(trim:%.trim.jpg=%.hough.png)
houghjson=$(trim:%.trim.jpg=%.hough.json)

snapshots=$(shell ls snapshots/*.pdf)
snapshotsjpg=$(snapshots:%.pdf=%.jpg)
dataimages=$(red) $(green) $(blue) $(gradientpng) $(contourspng) $(houghpng) $(lexpng) $(texpng)

alldata: $(contourssvg) $(contourspng) $(contoursjson) $(gradientsvg) $(gradientpng) $(gradientjson) $(red) $(redjson) $(green) $(greenjson) $(blue) $(bluejson) $(texsvg) $(texpng) $(texjson) $(lexsvg)  $(lexpng) $(lexjson) $(houghsvg) $(houghpng) $(houghjson)

snapshots: $(snapshotsjpg)
trim: $(trim)
contours: $(contourspng) $(contourssvg) $(contoursjson)
gradients: $(gradientsvg) $(gradientpng) $(gradientjson)
channels: $(red) $(green) $(blue) $(redjson) $(greenjson) $(bluejson)


zapzeros:
	find sample -size 0 -exec rm {} \;
cleandata:
	rm $(redjson) $(greenjson) $(bluejson) $(gradientjson) $(contoursjson)
cleancontourspng:
	rm $(contourspng)
cleangradientpng:
	rm $(gradientpng)


# sample.csv: csv/export_mim.csv
sample.csv: csv/mim.normalized.csv
	# head -n 101 $< > $@
	# scripts/csvrandom.py --shuffle --limit 100 --copyimages --outputpath sample $< $@ 
	scripts/csvrandom.py --copyimages --outputpath sample $< $@ 

%.trim.jpg: %.orig.jpg
	convert $< -gravity South -crop 0x0+0+25 $@

sample.json: sample.csv scripts/csv2json.py
	scripts/csv2json.py $< > $@

sample_with_data.json: sample.json $(contoursjson) $(gradientjson) $(redjson) $(greenjson) $(bluejson) $(texjson) $(lexjson) $(houghjson)
	scripts/joindata.py sample.json --inputglob "sample/*.json" > $@

# sample/exif.json:
# 	exiftool -FileModifyDate --json

# Contours
%.contours.svg: %.trim.jpg
	bin/contours $< tmp.png > $*.contours.svg
	rm tmp.png

%.contours.png: %.contours.svg
	inkscape --export-png=$@ $<

%.contours.json: %.contours.svg
	scripts/svgcountpaths.py $< --polyline --json --id $*.jpg --key contours --img $*.contours.png > $@

# Gradient
%.gradient.svg: %.trim.jpg
	scripts/imagegradient.py $< --svg $*.gradient.svg

%.gradient.png: %.gradient.svg
	inkscape --export-png=$*.gradient.png $<
	# inkscape --export-png=$*.gradient.png --export-background="#EEEEEE" $<

%.gradient.json: %.gradient.svg
	scripts/svgpathlength.py $< --json --id $*.jpg --key gradient --img $*.gradient.png > $@

# channels
%.red.png %.green.png %.blue.png: %.trim.jpg
	bin/channel $< $*.red.png $*.green.png $*.blue.png

%.red.json: %.red.png
	convert $< -format %c histogram:info:- | scripts/histcount.py --json --id $*.jpg --key red --img $*.red.png > $@

%.green.json: %.green.png
	convert $< -format %c histogram:info:- | scripts/histcount.py --json --id $*.jpg --key green --img $*.green.png > $@

%.blue.json: %.blue.png
	convert $< -format %c histogram:info:- | scripts/histcount.py --json --id $*.jpg --key blue --img $*.blue.png > $@

# texture + lex
%.tex.svg %.lex.svg: %.trim.jpg
	bin/texture $< $*.tex.svg $*.lex.svg /dev/null /dev/null /dev/null

%.tex.json: %.tex.svg
	scripts/svgcountpaths.py $< --json --id $*.jpg --key tex --img $*.tex.png --selector ".//{http://www.w3.org/2000/svg}g[@class='symbol']" > $@
%.lex.json: %.lex.svg
	scripts/svgcountpaths.py $< --json --id $*.jpg --key lex --img $*.tex.png --selector ".//{http://www.w3.org/2000/svg}g[@class='word']" > $@

%.tex.png: %.tex.svg
	inkscape --export-png=$@ $<

%.lex.png: %.lex.svg
	inkscape --export-png=$@ $<

# hough
%.hough.svg: %.trim.jpg
	bin/houghlines $< > $@

%.hough.json: %.hough.svg
	scripts/svgcountpaths.py $< --json --id $*.jpg --key hough --img $*.hough.png --selector ".//{http://www.w3.org/2000/svg}line" > $@

%.hough.png: %.hough.svg
	inkscape --export-png=$@ $<



%.jpg: %.pdf
	convert $< $@

%.tiles.pdf: %.pdf
	pdfposter -m A4 -p A1 $< $@

#
# POSTER
#

poster.pdf: sample_with_data.json scripts/orderings.poster.py $(dataimages)
	scripts/orderings.poster.py --filterzeros $< --output $@

poster2.pdf: sample_with_data.json scripts/orderings.poster.py $(dataimages)
	scripts/orderings.poster.py \
		--pagesize A1 --landscape \
		--columns 72 \
		--fontsize 7 \
		--filterzeros $< --output $@

poster3.pdf: sample_with_data.json scripts/orderings.poster.py $(dataimages)
	scripts/orderings.poster.py \
		--pagesize A1 --landscape \
		--columns 36 \
		--fontsize 7 \
		--filterzeros $< --output $@

poster4.pdf: sample_with_data.json scripts/orderings.poster.py $(dataimages)
	scripts/orderings.poster2.py \
		--pagesize A1 --landscape \
		--columns 101 \
		--fontsize 7 \
		--filterzeros $< --output $@

poster5.pdf: sample_with_data.json scripts/orderings.poster.columns.py $(dataimages)
	scripts/orderings.poster.columns.py \
		--pagesize A1 \
		--columns 39 \
		--fontsize 7 \
		--filterzeros $< --output $@
